# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 20:09:36 2020

@author: chris
"""

from flask import Flask
from flask import render_template

application = Flask(__name__)

@application.route('/')
def hello_world():
    return render_template('run_ml_model_inference.html')

if __name__ == '__main__':
    application.run(host='0.0.0.0')